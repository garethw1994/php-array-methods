<?php

    class Task {
        public $description;
        public $completed;

        public function __construct($description, $completed) {
            $this->description = $description;
            $this->completed = $completed;
        }
    }

    // methods
    function taskDescriptions ($tasks) {
        return array_column($tasks, 'description');
    };

    function completedTasks ($tasks) {
        return array_filter($tasks, function($task) {
            if ($task->completed) return $task;
        });
    };

    function updateTaskCompletion ($tasks) {
        return array_map(function($task) {

            $task->completed = !$task->completed;

            return $task;
        }, $tasks);
    };

    function addTask ($tasks, $newTask) {
        array_push($tasks, $newTask);

        return $tasks;
    };

    function removeDuplicates ($tasks) {
        return array_unique($tasks);

        // return $tasks;
    };

    // INITIALIZE SOME TASKS
    $tasks = [
        new Task("Buy groceries", true),
        new Task("Feed the dog", false),
        new Task("Learn about arrays in PHP", true),
        new Task("Meditate for 10 minutes", false),
        new Task('Wash the car', false)
    ];

     
    $newTasks = addTask($tasks, new Task('Wash the car', false));

  
    var_dump(removeDuplicates($newTasks));
